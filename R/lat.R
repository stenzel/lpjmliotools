#' LPJmL latitude
#'
#' This file contains the latitude of the midpoint for each grid cell 
#'
#' @docType data
#'
#' @usage data(lat)
#'
#' @format An array of dim (67420), with the latitude for each LPJmL grid cell.
#'
#' @keywords datasets
#'
#' @references See LPJmL gitlab wiki
#' (\href{https://gitlab.pik-potsdam.de/lpjml/LPJmL_internal/wikis/Output}{LPJmL wiki Output})
#'
#' @source contact Fabian Stenzel
#'
#' @examples
#' data(lat)
"lat"
