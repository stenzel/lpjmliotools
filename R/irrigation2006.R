#' LPJmL irrigation data from 2006
#'
#' This is example data from LPJmL containing 
#' global irrigation water amounts in mm/yr
#'
#' @docType data
#'
#' @usage data(irrigation2006)
#'
#' @format An array of dim (67420), with irrig amount for each LPJmL grid cell.
#'
#' @keywords datasets
#'
#' @references See LPJmL gitlab wiki
#' (\href{https://gitlab.pik-potsdam.de/lpjml/LPJmL_internal/wikis/Irrigation}{LPJmL wiki})
#'
#' @source contact Fabian Stenzel
#'
#' @examples
#' data(irrigation2006)
"irrigation2006"
