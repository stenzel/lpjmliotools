# lpjmliotools


*The goal of lpjmliotools is to provide functions to read, write, process, and plot 
[LPJmL-related](https://gitlab.pik-potsdam.de/lpjml/LPJmL_internal) data.
Most of the functionality is recently also provided by the
[lpjmlkit package](https://gitlab.pik-potsdam.de/lpjml/lpjmlkit).*

## Installation

You can install `lpjmliotools` by git cloning this repository:

```bash
git clone https://gitlab.pik-potsdam.de/stenzel/lpjmliotools.git <path_to_lpjmliotools>
```

and install via  [`devtools`](https://rawgit.com/rstudio/cheatsheets/master/package-development.pdf):

```R
devtools::install("<path_to_lpjmliotools>")
library("lpjmliotools")
```

alternatively, you can also load it from source:

```R
devtools::load_all("<path_to_lpjmliotools>")
```

## Examples
